﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATMClient
{
    public partial class ConfirmTransferForm : Form
    {
        public bool result;

        public ConfirmTransferForm(string targetAccountId, float cash)
        {
            InitializeComponent();
            this.textBox_accountId.Text = targetAccountId;
            this.label_cash.Text += (cash.ToString() + " to account with following id:");
        }

        private void button_yes_Click(object sender, EventArgs e)
        {
            this.result = true;
            this.Close();
        }

        private void button_no_Click(object sender, EventArgs e)
        {
            this.result = false;
            this.Close();
        }
    }
}
