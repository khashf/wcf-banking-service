﻿namespace ATMClient
{
    partial class ConfirmTransferForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_cash = new System.Windows.Forms.Label();
            this.textBox_accountId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_yes = new System.Windows.Forms.Button();
            this.button_no = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_cash
            // 
            this.label_cash.AutoSize = true;
            this.label_cash.Location = new System.Drawing.Point(13, 13);
            this.label_cash.Name = "label_cash";
            this.label_cash.Size = new System.Drawing.Size(122, 14);
            this.label_cash.TabIndex = 0;
            this.label_cash.Text = "Are you sure to tranfer ";
            // 
            // textBox_accountId
            // 
            this.textBox_accountId.Location = new System.Drawing.Point(16, 31);
            this.textBox_accountId.Name = "textBox_accountId";
            this.textBox_accountId.ReadOnly = true;
            this.textBox_accountId.Size = new System.Drawing.Size(256, 20);
            this.textBox_accountId.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(231, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "Your action can\'t rollback after this transaction";
            // 
            // button_yes
            // 
            this.button_yes.Location = new System.Drawing.Point(229, 87);
            this.button_yes.Name = "button_yes";
            this.button_yes.Size = new System.Drawing.Size(75, 23);
            this.button_yes.TabIndex = 3;
            this.button_yes.Text = "Yes";
            this.button_yes.UseVisualStyleBackColor = true;
            this.button_yes.Click += new System.EventHandler(this.button_yes_Click);
            // 
            // button_no
            // 
            this.button_no.Location = new System.Drawing.Point(148, 87);
            this.button_no.Name = "button_no";
            this.button_no.Size = new System.Drawing.Size(75, 23);
            this.button_no.TabIndex = 4;
            this.button_no.Text = "No";
            this.button_no.UseVisualStyleBackColor = true;
            this.button_no.Click += new System.EventHandler(this.button_no_Click);
            // 
            // ConfirmTransferForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 122);
            this.Controls.Add(this.button_no);
            this.Controls.Add(this.button_yes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_accountId);
            this.Controls.Add(this.label_cash);
            this.MaximizeBox = false;
            this.Name = "ConfirmTransferForm";
            this.Text = "Confirm Your Transferation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_cash;
        private System.Windows.Forms.TextBox textBox_accountId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_yes;
        private System.Windows.Forms.Button button_no;
    }
}