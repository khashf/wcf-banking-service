﻿namespace ATMClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_loadInfo = new System.Windows.Forms.Button();
            this.richTextBox_info = new System.Windows.Forms.RichTextBox();
            this.button_withdraw = new System.Windows.Forms.Button();
            this.numeric_withdraw = new System.Windows.Forms.NumericUpDown();
            this.button_transfer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_targetId = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_transfer = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button_logout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_withdraw)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_transfer)).BeginInit();
            this.SuspendLayout();
            // 
            // button_loadInfo
            // 
            this.button_loadInfo.Location = new System.Drawing.Point(12, 170);
            this.button_loadInfo.Name = "button_loadInfo";
            this.button_loadInfo.Size = new System.Drawing.Size(260, 23);
            this.button_loadInfo.TabIndex = 0;
            this.button_loadInfo.Text = "Load Info";
            this.button_loadInfo.UseVisualStyleBackColor = true;
            this.button_loadInfo.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox_info
            // 
            this.richTextBox_info.Location = new System.Drawing.Point(12, 12);
            this.richTextBox_info.Name = "richTextBox_info";
            this.richTextBox_info.ReadOnly = true;
            this.richTextBox_info.Size = new System.Drawing.Size(260, 152);
            this.richTextBox_info.TabIndex = 1;
            this.richTextBox_info.Text = "";
            // 
            // button_withdraw
            // 
            this.button_withdraw.Location = new System.Drawing.Point(9, 70);
            this.button_withdraw.Name = "button_withdraw";
            this.button_withdraw.Size = new System.Drawing.Size(251, 23);
            this.button_withdraw.TabIndex = 2;
            this.button_withdraw.Text = "Withdraw cash";
            this.button_withdraw.UseVisualStyleBackColor = true;
            this.button_withdraw.Click += new System.EventHandler(this.button_withdraw_Click);
            // 
            // numeric_withdraw
            // 
            this.numeric_withdraw.Increment = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numeric_withdraw.Location = new System.Drawing.Point(9, 44);
            this.numeric_withdraw.Name = "numeric_withdraw";
            this.numeric_withdraw.ReadOnly = true;
            this.numeric_withdraw.Size = new System.Drawing.Size(251, 20);
            this.numeric_withdraw.TabIndex = 3;
            // 
            // button_transfer
            // 
            this.button_transfer.Location = new System.Drawing.Point(6, 92);
            this.button_transfer.Name = "button_transfer";
            this.button_transfer.Size = new System.Drawing.Size(251, 23);
            this.button_transfer.TabIndex = 4;
            this.button_transfer.Text = "Transfer";
            this.button_transfer.UseVisualStyleBackColor = true;
            this.button_transfer.Click += new System.EventHandler(this.button_transfer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "Transfer to user with id";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox_targetId);
            this.groupBox1.Controls.Add(this.button_transfer);
            this.groupBox1.Controls.Add(this.numericUpDown_transfer);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 317);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 122);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Transfer money";
            // 
            // textBox_targetId
            // 
            this.textBox_targetId.Location = new System.Drawing.Point(134, 22);
            this.textBox_targetId.Name = "textBox_targetId";
            this.textBox_targetId.Size = new System.Drawing.Size(126, 20);
            this.textBox_targetId.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.button_withdraw);
            this.groupBox2.Controls.Add(this.numeric_withdraw);
            this.groupBox2.Location = new System.Drawing.Point(12, 199);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 112);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Withdraw cash";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "Amount cash to withdraw";
            // 
            // numericUpDown_transfer
            // 
            this.numericUpDown_transfer.Increment = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numericUpDown_transfer.Location = new System.Drawing.Point(9, 66);
            this.numericUpDown_transfer.Name = "numericUpDown_transfer";
            this.numericUpDown_transfer.ReadOnly = true;
            this.numericUpDown_transfer.Size = new System.Drawing.Size(251, 20);
            this.numericUpDown_transfer.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "with amount of cash:";
            // 
            // button_logout
            // 
            this.button_logout.Location = new System.Drawing.Point(18, 445);
            this.button_logout.Name = "button_logout";
            this.button_logout.Size = new System.Drawing.Size(251, 23);
            this.button_logout.TabIndex = 8;
            this.button_logout.Text = "Logout";
            this.button_logout.UseVisualStyleBackColor = true;
            this.button_logout.Click += new System.EventHandler(this.button_logout_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 476);
            this.Controls.Add(this.button_logout);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox_info);
            this.Controls.Add(this.button_loadInfo);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "ATM";
            ((System.ComponentModel.ISupportInitialize)(this.numeric_withdraw)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_transfer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_loadInfo;
        private System.Windows.Forms.RichTextBox richTextBox_info;
        private System.Windows.Forms.Button button_withdraw;
        private System.Windows.Forms.NumericUpDown numeric_withdraw;
        private System.Windows.Forms.Button button_transfer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_targetId;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_transfer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_logout;
    }
}

