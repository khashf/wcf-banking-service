﻿using ATMClient.ACBBankingService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATMClient
{
    public partial class Login : Form
    {
        public Account returnAccount { get; set; }
        public string bindingType;
        private const string WSHTTP_BINDING = "WSHttpBinding_IBankingService";
        private const string NETTCP_BINDING = "NETTCPBinding_IBankingService";

        public Login()
        {
            InitializeComponent();
            this.bindingType = WSHTTP_BINDING;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BankingServiceClient ACBBankingService = new BankingServiceClient(bindingType);
            this.returnAccount = ACBBankingService.GetAccount(this.textBox_cardNumber.Text, this.textBox_cardPin.Text);
            if (this.returnAccount == null)
            {
                this.textBox_cardNumber.Text = "";
                this.textBox_cardPin.Text = "";
                MessageBox.Show("Card Number of Card PIN is incorrect");
            }
            else
            {
                this.Close();
            }

        }

        private void radioButton_wsHttp_CheckedChanged(object sender, EventArgs e)
        {
            //this.radioButton_wsHttp.Checked = true;
            //this.radioButton_netTcp.Checked = false;
            this.bindingType = WSHTTP_BINDING;
        }

        private void radioButton_netTcp_CheckedChanged(object sender, EventArgs e)
        {
            //this.radioButton_netTcp.Checked = true;
            //this.radioButton_wsHttp.Checked = false;
            this.bindingType = NETTCP_BINDING;
        }
    }
}
