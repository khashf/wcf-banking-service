﻿namespace ATMClient
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_cardNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_cardPin = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton_netTcp = new System.Windows.Forms.RadioButton();
            this.radioButton_wsHttp = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Card Number";
            // 
            // textBox_cardNumber
            // 
            this.textBox_cardNumber.Location = new System.Drawing.Point(16, 31);
            this.textBox_cardNumber.Name = "textBox_cardNumber";
            this.textBox_cardNumber.Size = new System.Drawing.Size(206, 20);
            this.textBox_cardNumber.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "PIN Number";
            // 
            // textBox_cardPin
            // 
            this.textBox_cardPin.Location = new System.Drawing.Point(16, 76);
            this.textBox_cardPin.Name = "textBox_cardPin";
            this.textBox_cardPin.PasswordChar = '*';
            this.textBox_cardPin.Size = new System.Drawing.Size(206, 20);
            this.textBox_cardPin.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(206, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton_netTcp);
            this.groupBox1.Controls.Add(this.radioButton_wsHttp);
            this.groupBox1.Location = new System.Drawing.Point(16, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 47);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // radioButton_netTcp
            // 
            this.radioButton_netTcp.AutoSize = true;
            this.radioButton_netTcp.Location = new System.Drawing.Point(107, 19);
            this.radioButton_netTcp.Name = "radioButton_netTcp";
            this.radioButton_netTcp.Size = new System.Drawing.Size(92, 18);
            this.radioButton_netTcp.TabIndex = 6;
            this.radioButton_netTcp.Text = "netTcpBinding";
            this.radioButton_netTcp.UseVisualStyleBackColor = true;
            // 
            // radioButton_wsHttp
            // 
            this.radioButton_wsHttp.AutoSize = true;
            this.radioButton_wsHttp.Checked = true;
            this.radioButton_wsHttp.Location = new System.Drawing.Point(6, 19);
            this.radioButton_wsHttp.Name = "radioButton_wsHttp";
            this.radioButton_wsHttp.Size = new System.Drawing.Size(95, 18);
            this.radioButton_wsHttp.TabIndex = 7;
            this.radioButton_wsHttp.TabStop = true;
            this.radioButton_wsHttp.Text = "wsHttpBinding";
            this.radioButton_wsHttp.UseVisualStyleBackColor = true;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 209);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_cardPin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_cardNumber);
            this.Controls.Add(this.label1);
            this.Name = "Login";
            this.Text = "Login";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_cardNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_cardPin;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton_netTcp;
        private System.Windows.Forms.RadioButton radioButton_wsHttp;
    }
}