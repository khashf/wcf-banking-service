﻿using ATMClient.ACBBankingService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ATMClient
{
    public partial class Form1 : Form
    {
        private Account account;
        private BankingServiceClient ACBBankingService;

        private const float transferCharge = 3000.0f;
        private const float minimumBalance = 50000.0f;

        private string bindingType;

        public Form1()
        {
            
            using (Login loginForm = new Login())
            {
                loginForm.ShowDialog();
                this.account = loginForm.returnAccount;
                this.bindingType = loginForm.bindingType;
                ACBBankingService = new BankingServiceClient(this.bindingType);
                InitializeComponent();
                this.numeric_withdraw.Maximum = decimal.MaxValue;
                this.numericUpDown_transfer.Maximum = decimal.MaxValue;
                loadInfo();
            }
        }

        private void loadInfo()
        {
            this.richTextBox_info.Text = "";
            this.richTextBox_info.Text += (this.account.AccountId + "\n"
                                            + this.account.Username + "\n"
                                            + this.account.ExpiredDate.ToString() + "\n"
                                            + this.account.Balance.ToString() + "\n"
                                            + "-----------------------\n");
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.loadInfo();
        }

        private void button_withdraw_Click(object sender, EventArgs e)
        {
            float cash = (float)this.numeric_withdraw.Value;
            if (this.account.Balance - cash < minimumBalance)
            {
                MessageBox.Show("Remain balance after withdrawal must be at least 50000");
            }
            else
            {
                // withdraw cash
                Account tempAccount = this.ACBBankingService.UpdateCash(this.account.AccountId, -cash);
                if (tempAccount == null)
                {
                    MessageBox.Show("Withdrawal failed");
                }
                else
                {
                    this.account = tempAccount;
                    MessageBox.Show("Withdrawed successfully");
                    this.loadInfo();
                }
            }
        }

        private void button_transfer_Click(object sender, EventArgs e)
        {
            // Get cash
            float cash = (float)this.numericUpDown_transfer.Value;
            // Check balance after transferation
            if (this.account.Balance - cash - transferCharge < minimumBalance)
            {
                MessageBox.Show("Remain balance after withdrawal must be at least 50000");
                return;
            }
            // Get target user's id
            string targetUserId = this.textBox_targetId.Text;
            Account targetAccount = this.ACBBankingService.Find(targetUserId);
            if (targetAccount == null)
            {
                MessageBox.Show("Can't find target user\nTransferation failed");
                return;
            }
            // Confirm target account
            using (ConfirmTransferForm confirmForm = new ConfirmTransferForm(targetUserId, cash))
            {
                confirmForm.ShowDialog();
                if (confirmForm.result == false)
                {
                    return;
                }
                
            }

            // Withdraw cash from this account
            Account tempSenderAccount = this.ACBBankingService.UpdateCash(this.account.AccountId, -cash);
            if (tempSenderAccount == null)
            {
                MessageBox.Show("Failed to withdraw cash from your account");
                return;
            }
            Account tempRecieverAccount = this.ACBBankingService.UpdateCash(targetUserId, cash);
            if (tempRecieverAccount == null)
            {
                MessageBox.Show("Failed to transfer cash to target account");
                return;
            }
            this.account = tempSenderAccount;
            MessageBox.Show("Transferation succecced");
            this.loadInfo();
        }

        private void button_logout_Click(object sender, EventArgs e)
        {
            using (Login loginForm = new Login())
            {
                this.Hide();
                loginForm.ShowDialog();

                this.account = loginForm.returnAccount;
                this.Show();

                ACBBankingService = new BankingServiceClient(this.bindingType);
                loadInfo();
            }
            
        }
    }
}
