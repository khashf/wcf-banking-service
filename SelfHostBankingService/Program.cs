﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace SelfHostBankingService
{
    class Program
    {
        static private string shutdownPassword = "trust me im engineer";
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(BankingServiceLib.BankingService));
            host.Open();
            //Console.WriteLine("Host is runing, press any key to stop...");
            //Console.ReadLine();
            //host.Close();
            while (true)
            {
                Console.WriteLine("Input password to turn off service: ");
                if (Console.ReadLine() == shutdownPassword)
                {
                    Console.WriteLine("Password corrected, service is shutting down..");
                    host.Close();
                    break;
                }
                else
                {
                    Console.WriteLine("Wrong password");
                }
            }
            
        }
    }
}
