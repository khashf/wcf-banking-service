﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BankingServiceLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class BankingService : IBankingService
    {
        private DataAccessUtil acbBankDataAccess = new DataAccessUtil("acbbank");

        public Account GetAccount(string accountId, string password)
        {
            Account account = acbBankDataAccess.load(accountId);
            if (account == null)
            {
                return null;
            }
            if (account.Password != password)
            {
                return null;
            }
            return account;
        }

        public Account UpdateCash(string accountId, float cash)
        {
            Account updatedAccount = acbBankDataAccess.update(accountId, cash);
            if (updatedAccount == null)
            {
                return null;
            }
            return updatedAccount;
        }

        public Account Find(string accountId)
        {
            return acbBankDataAccess.load(accountId);
        }

        // Not use
        public Account Transfer(string senderId, string receiverId, float cash)
        {
            return null;
        }
    }
}
