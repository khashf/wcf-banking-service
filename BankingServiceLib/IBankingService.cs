﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BankingServiceLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IBankingService
    {
        [OperationContract]
        Account GetAccount(string accountId, string password);

        [OperationContract]
        Account UpdateCash(string accountId, float amountOfCash);

        [OperationContract]
        Account Transfer(string senderId, string receiverId, float amountOfCash);

        [OperationContract]
        Account Find(string accountId);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Account
    {
        private string accountId;
        private string password;
        private string username;
        private DateTime expiredDate;
        private float balance;

        [DataMember]
        public string AccountId
        {
            get { return accountId; }
            set { accountId = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        [DataMember]
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        [DataMember]
        public DateTime ExpiredDate
        {
            get { return expiredDate; }
            set { expiredDate = value; }
        }

        [DataMember]
        public float Balance
        {
            get { return balance; }
            set { balance = value; }
        }


    }
}
