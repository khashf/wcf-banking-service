﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BankingServiceLib
{
    public class DataAccessUtil
    {
        private SqlConnection connection;
        public string databaseName;
        public DataAccessUtil(string databaseName)
        {
            this.databaseName = databaseName;
            connection = new SqlConnection(@"Data Source=CAMKHUONG;Initial Catalog=" + databaseName + ";Integrated Security=True");
        }

        
        public Account load(string accountId)
        {
            Account account = new Account();
            string query = "Select * from dbo.Accounts"
                            + " where userid = '"
                            + accountId
                            + "'";
            SqlCommand cmd = new SqlCommand(query, connection);
            connection.Open();
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                if (!reader.Read())
                {
                    return null;
                }
                account.AccountId = reader[1].ToString();
                account.Password = reader[2].ToString();
                account.Username = reader[3].ToString();
                account.ExpiredDate = DateTime.Parse(reader[4].ToString());
                account.Balance = float.Parse(reader[5].ToString());
                connection.Close();
            }
            catch (Exception e)
            {
                connection.Close();
                return null;
            }
            return account;
        }

        public Account update(string accountId, float cash)
        {
            Account updatedAccount = this.load(accountId);
            updatedAccount.Balance += cash;
            string query = "Update dbo.Accounts"
                            + " Set balance="
                            + updatedAccount.Balance
                            + " where userid = '"
                            + accountId
                            + "'";
            SqlCommand cmd = new SqlCommand(query, connection);
            connection.Open();
            try
            {
                int effectRows = cmd.ExecuteNonQuery();
                connection.Close();
                if (effectRows <= 0)
                    return null;
                else
                    return updatedAccount;
            }
            catch (Exception e)
            {
                connection.Close();
                return null;
            }
        }

    }
}
